# Copyright (C) 2017  Marc Nieper-Wißkirchen

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

LIBRARY_FILE = rapid/error-message.sld

SNOW = snow-chibi.scm

PACKAGE = `echo $(LIBRARY_FILE) | sed -e 's/\.sld//' -e 's/\//-/g'`
LIBRARY = "(`echo $(LIBRARY_FILE) | sed -e 's/\.sld//' -e 's/\// /g'`)"

package:
	@$(SNOW) package $(LIBRARY_FILE)

install: package
	@$(SNOW) install $(PACKAGE)-`cat VERSION`.tgz

upload: package
	@$(SNOW) upload $(PACKAGE)-`cat VERSION`.tgz

remove:
	@$(SNOW) remove $(LIBRARY)

test:	package
	@$(SNOW) install --install-prefix=build --show-tests $(PACKAGE)-`cat VERSION`.tgz

clean:
	@rm -rf *.tgz
	@rm -rf build
	@rm -f library.sld
